const express = require("express");
const port = 8000;
const ip = "0.0.0.0";
const app = express();
let jsonUrl = "";
const http = require('http');
const { Client } = require('node-osc');
let oscAddress = "";
var path = require('path');
const pkgDir = require('pkg-dir');
const homedir = require('os').homedir();
var dgram = require('dgram');
let delay = 0;
let timeoutObj;

let jsonData = require(homedir+'/venice/config.json');
const rootDir = pkgDir(__dirname);
console.log(jsonData);
jsonUrl = jsonData['url'];
delay = jsonData['delay'];
oscAddress = jsonData['oscAddress'];

const clientOSC = new Client(jsonData['oscHost'], jsonData['oscPort']);

function sendOSC(valore){
  // var message = new Buffer(valore+'');
  clientOSC.send(oscAddress, valore, () => {
    console.log("Get new value");
  });
  // var client = dgram.createSocket('udp4');
  // client.send(message, 0, message.length, jsonData['oscPort'], jsonData['oscHost'], function(err, bytes) {
  //     if (err) throw err;
  //     console.log('UDP message sent to ' + jsonData['oscHost'] +':'+ jsonData['oscPort']);
  //     client.close();
  // });

}

function getRemoteJSON(url){
  http.get(url, (resp) => {
    let data = '';
    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });

    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      const jsonData = JSON.parse(data);
      const valore = parseFloat(jsonData[0]['valore']);
      sendOSC(valore);
      clearTimeout(timeoutObj);
      timeoutObj = setTimeout(getData, delay);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
    clearTimeout(timeoutObj);
    timeoutObj = setTimeout(getData, delay);
  });
}


function getData(){
  getRemoteJSON(jsonUrl);
}

app.use(express.static('public'))

app.get("/", function(req, res)
{
  // res.send("Hi!");
  res.sendFile(__dirname + "/index.html");
})

app.get("/test", function(req, res)
{
  res.sendFile(__dirname + "/test.html");
})

const server = app.listen(port, ip, function()
{
  console.log("server started on "+ip+":"+port);
  getData();
})